# Implementation

## Make backup of the following directories
E:/inetpub/wwwroot/$REDACTED.Service.Internal.DynamicRouting
E:/inetpub/wwwroot/$REDACTED.Web

## Kafka
### Log on to c2ks21, and create topics
kaka-topics --create --zookeeper c2kz21.$REDACTED.com:2181,c2kz22.$REDACTED.com:2181,c2kz23.lab.$REDACTED.com:2181 --topic $REDACTED.STAGE1.TRACKPOINT --replication-factor 2 --partitions 40

### To verify topic creation, run the following
kafka-topics --describe --zookeeper c2kz21.$REDACTED.com:2181,c2kz22.$REDACTED.com:2181,c2kz23.lab.$REDACTED.com:2181 --topic $REDACTED.STAGE1.TRACKPOINT

​## Database

### From DOS command type
SQLPLUS $REDACTED/$REDACTED@C2DBX00

### Run the following command (wail until all bat files to complete before moving to the next step)
UPGRADE-PROD 

### Run the following command
UPGRADE-PROD-2

### Check for errors
Go into LOGS folder in each DDLS,DMLS,PACKAGES,PROCEDURES,VIEWS,TRIGGERS folders and check for ORACLE errors in log files.

### Log into database and verify any invalid objects. If there are invalid objects then run step #6 otherwise the upgrade is completed
SQLPLUS $REDACTED/$REDACTED@c2dbx00
SELECT owner,object_name FROM all_objects where status <> 'VALID' and owner not like '%SYS%';

### Recompile the entire database
SQLPLUS $REDACTED/$REDACTED@C2DBX00
@recompile.sql;


## $REDACTED Web

### The following steps have to be done on 02, 05, 07, 08 first - once completed IT Network will flip
those with 01, 03, 04, 06 and then we will deploy to those 

### Navigate to the following links
http://10.36.130.45:8080/jenkins/view/production%20c2%20servers/
http://10.36.130.45:8080/jenkins/view/production%20c1%20servers/

### Start the job 
c2v11web0*, c1v11web0*   //////// THIS JOB DOES NOT EXIST FOR PRODUCTION

### Add these changes to the this file
E:/inetpub/wwwroot/$REDACTED.Service.Internal.DynamicRouting/web.config

---
      <setting name="SVGPath" serializeAs="String"> 
        <value>E:\inetpub\wwwroot\$REDACTED.Web\Images\Mugshots\svg\</value> 
      </setting>
      <setting name="UseProxy" serializeAs="String"> 
        <value>false</value> 
      </setting> 
---

## backend install/upgrade steps

### ssvc servers (c2ssvc01, c2ssvc02)

#### logon as root
sudo -i

#### Update and Upgrade packages
yum update STOP_Database_SSVC_Msg_Route
yum upgrade STOP_smsclientengine

### etl servers (c2etl01, c2etl02, c2etl03, c2etl04, c2etl05, c2etl06)

#### Logon as etl user
su - etl

#### Kill several services
Kill tdcchmon 
Kill vtclient

#### Wait for ETL1Q to clear, may need to manually flip ETL1Q P records to A
q stat

#### Install, update, upgrade packages
su -
yum update STOP_Database_ETLS1
yum install STOP_Shared_Elastic_Client
yum upgrade STOP_ETL_Binaries-vtstage1processor
yum upgrade STOP_ETL_Binaries-vtclient
yum install STOP_Elasticsync
yum install STOP_ETL_Binaries-stage1trackpointprocessor
yum update STOP_ETL_Configs
yum update STOP_ETL_Scripts

##### NOTE
sync_client_start=a
sync_client_end =z

is to load balance the sync between zones in client schemas and elastic namespace.
In integrate, you want to set following for i1etl01,
sync_client_start=a
sync_client_ end =l

and followings for i1etl02
sync_client_start=m
sync_client_ end =z

On production environments, you want to distribute workload of sync-ing client schemas among 5 etl servers,

A rough estimate might go as 
etl01: a-b
etl02: c-l
etl03: m-t
etl04: u-z

#### After install is complete and ini is configured, please complete the following steps on etl servers

Stop tdcchmon and elasticsync process if started already.
Remove all files from /usr/local/etl/elasticsearch/map and /usr/local/etl/elasticsearch/tmp directory

#### remove configurations for TNJUV1, TN2, TN1
cd to elasticsearch/config
rm files end with
. TNJUV1.config
. TN2.config
. TN1.config

#### Sync with elasticsearch
./elasticsync -t

#### Verify all configurations before proceeding
scripts/app_list.lst
hmon.cfg
etl.ini
tdcc.ini

#### Start the etl stack
TS

### On gateway servers (c2gw01, c2gw02, c2gw03, c2gw04, c2gw05)

#### Logon on as tdcc
su - tdcc
Kill tdcchmon
Kill $REDACTED and let it finish processing VTQ

#### Upgrade packages
su -
yum upgrade STOP_Gateway_Binaries-vtcommunicator
yum upgrade STOP_Gateway_Binaries-$REDACTED
yum upgrade STOP_Gateway_Binaries-smscommunicator

#### Return to tdcc user
su - tdcc

#### restart the stack
TK
TS

# Verification
​
## Database:

### verify build successful​
03_Database_Integrate   //////// Since this step will be manual, how will we verify success?

​## $REDACTED Web

### Verify all steps completed succesfully in jenkins

# Backout

## Database

revert to branch "Develop"

### checkout develop branch 

http://bitbucket.dal.$REDACTED.net/projects/STOP/repos/database/browse?at=refs%2Fheads%2FDevelop

### apply upgrade script​      //////// Should we elaborate?

​## $REDACTED Web

### Replace folders with backups

E:/inetpub/wwwroot/$REDACTED.Service.Internal.DynamicRouting
E:/inetpub/wwwroot/$REDACTED.Web
