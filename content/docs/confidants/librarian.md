# The Librarian
## RECORD
---
```
Name: $REDACTED
Alias: ['The Librarian', and 55 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: N/A
Chronological Age: N/A
SCAN Rank: | B B
           | A C
TIIN Rank: | C A
           | A C
Reviewer Rank: 3 stars
Organizations:
  - The Resistance
Occupations:
  - Librarian
Relationships:
  - The Cartographer
  - The Dave
Variables:
  $WOKE: +0.60 | # At least partially.
```

## ECO
---
The Librarian is responsible for the cataloguing and classification of all knowledge - both true and false. He is a broker of ideas, guiding others into their own personal enlightenment. 

While he does harbor her own opinions, he is primarily responsible for the [Integration](/posts/theories/integration) of ideas. He is responsible for understanding most areas of knowledge at the [Overview Effect](/posts/theories/overview-effect) level.