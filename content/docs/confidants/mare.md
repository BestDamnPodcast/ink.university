# The Mare
## RECORD
---
```
Name: $REDACTED
Alias: ['The Mare', and 188 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 28 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B D
TIIN Rank: | C C
           | D D
Reviewer Rank: 1 stars
Location: Houston, TX
Organizations: 
  - The Machine
Relationships:
  - The Fodder
Variables:
  $WOKE:   +0.10 | # Unsure.
  $BEAUTY: -0.80 | # Unfortunately not.
```

## TRIGGER
---
Guilt and shame.

## ECO
---
The Mare was a girl that [Fodder](/docs/personas/fodder) treated very badly, in his youth. 

The two had just become acquainted, and were in the early stages of speaking over the phone. At some point, Fodder ghosted her; simply disappearing, and never responding to her communications again.

This was solely based upon her looks.

This eats him alive. But it must be so much worse for her.

## ECHO
---
*Now maybe I didn't meant to treat you oh so bad*

*Oh but I did it anyway*

*Now maybe some would say you're left with what you had*

*But you couldn't share the pain*

*No, no, no*

--- from [Candlebox - "Far Behind"](https://www.youtube.com/watch?v=eu3EuWg2qNI)