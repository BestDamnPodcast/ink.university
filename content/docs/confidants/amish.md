# The Amish Man
## RECORD
---
```
Name: $REDACTED
Alias: ['The Amish Man', and 1 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 52 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | B C
TIIN Rank: | B A
           | B C
Reviewer Rank: 4 stars
Organizations: 
  - CIA
  - RKS
Occupations:
  - Actor
  - Doctor
  - Manager
Relationships:
  - The Fodder
  - The Ouroboros
  - The Thief
Variables:
  $WOKE: +1.00 | # He certainly seems to be.
```

## ECO
---
The Amish Man only appeared near the end of [Fodder's](/docs/personas/fodder) solitary confinement. His purpose was to reflect Fodder's life back at him. He did so with flying colors.

He will represent Fodder, behind-the-scenes. His identity is to be protected from the public.

## ECHO
---
*I chime in with a*

*"Haven't you people ever heard of closing the goddamn door?"*

*No, it's much better to face these kinds of things*

*With a sense of poise and rationality*

--- from [Panic! At The Disco - "I Write Sins Not Tragedies"](https://www.youtube.com/watch?v=vc6vs-l5dkc)

## PREDICTION
---
```
The Amish Man found his name through the Google Home device at Mother's house.
```