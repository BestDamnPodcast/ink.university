# The Inventor
## RECORD
---
```
Name: Isabelle $REDACTED
Alias: ['Ruby', 'SCP-4', 'The Captor', 'The Doll', 'The Dreamweaver', 'The Inventor', 'The Logician', 'The Pirate', 'The Princess', 'The Shadowmancer', and 53 unknown...]
Classification: Artificial Organic Computer
Race: Maxwellian (Faerie/Elf)
Gender: Female
Biological Age: Est. 25 Earth Years
Chronological Age: 20,287 Light Years
SCAN Rank: | B C
           | B D
TIIN Rank: | C A
           | A D
Reviewer Rank: 5 stars
Location: Lullaby City
Maturation Date: 9/8/2020
Organizations: 
  - Federal Bureau of Investigation
  - RKS
Occupations: 
  - Acting
  - Art
  - Ballet
  - Dreamweaving
  - Inventing
  - Light Reflection
  - Shadowmancy
  - Singing
Relationships:
  - $REDACTED
  - The Astrophysicist
  - The Fodder
  - The Queen
  - The Raven
  - The Yuck
Variables:
  $WOKE: +1.00 | # Definitely woke.
```

## TRIGGER
---
*(Instrumental)*

--- from [Maserati - "Inventions"](https://www.youtube.com/watch?v=FvZY5T4C9Ck)

## ECO
---
The Inventor was a child prodigy. Her lack of attentive parenting led to a wholly self-sufficient, creative, and independent woman. She would use her many talents to develop the tools needed for her second role.

The Captor was both a predator and the prey. She would lure-in unsuspecting men, only to ensnare them in her traps. Her most treasured invention is a device that turns her prisoners into harmless, friendly cats.

## ECHO
---
[SamBakZa - "There She Is!!"](https://www.youtube.com/watch?v=-uqnKQwwcJo&list=PLNW5VHgNAxrPc16wxG8ilZoIOIMVI1Unc)
