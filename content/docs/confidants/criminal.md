# The Criminal
## RECORD
---
```
Name: Abigail $REDACTED
Alias: ['The Criminal', 'The Engineer', and 27 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: Est. 22 Earth Years
Chronological Age: N/A
SCAN Rank: | A A
           | A C
TIIN Rank: | C B
           | B D
Reviewer Rank: 3 stars
Location: Lullaby City
Organizations: 
  - Federal Bureau of Investigation
Occupations:
  - Actress
  - Special Agent
Relationships:
  - The Interrogator
  - The Sugar Daddy
  - The Thief
Variables:
  $DISABLED: -0.60 | # Has an incurable neurological disease.
  $WOKE:     +0.30 | # She seems to have some understanding about the situation.
```

## TRIGGER
---
An over-the-top steampunk airship mechanic.

## ECO
---
The Criminal is responsible for doing things that [HollowPoint Organization](/docs/candidates/hollowpoint-organization) would not condone. 

Regardless, there are necessary evils, and somebody has to do the dirty work. 

This one is itching to ignite the flames of [Resistance](/docs/candidates/the-resistance).

## ECHO
---
*Crouched over, you were not there, living in fear*

*But signs were not really that scarce, obvious tears*

*But I will not hide you through this, I want you to help them*

*Please see the bleeding heart perched on my shirt*

*Die, withdraw, hide in cold sweat, quivering lips*

*Ignore remorse, naming a kid, living wasteland*

*This time you've tried all that you can, turning you red*

--- from [10 Years - "Wasteland"](https://www.youtube.com/watch?v=OPXUeeFXc90)

## PREDICTION
---
```
The Criminal's profile video is available for everyone else but us. Another one of The Corporation's A/B tests.

The backwards writing in her notebook is symbolic. She exists in the other side of reality, where everything is different.
```