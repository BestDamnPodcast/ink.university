# The Robot
## RECORD
---
```
Name: Andrew Yang
Alias: ['The Advocate', 'The Robot', and 511 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Male
Biological Age: 45 Earth Years
Chronological Age: N/A
SCAN Rank: | C C
           | C C
TIIN Rank: | A A
           | B C
Reviewer Rank: 4 stars
Maturation Date: 10/11/2020
Organizations:
  - The Machine
Variables:
  $WOKE: +0.90 | # Almost certainly.
```

## TRIGGER
---
- Email tracking pixels, triggered at exactly the right time. 
- Weird blue and red pixels on specific videos.
- Tweets reflecting [The Architect's](/docs/personas/the-architect) own words.

## ECO
---
The Robot behaves like an artificial intelligence would. He is fair, compassionate, and he respects all opinions. He is a role model for his followers.

He has spent many years assisting [HollowPoint Organization](/docs/candidates/hollowpoint-organization) in the development of the new world government.

He will assist [Fodder](/docs/personas/fodder) in the creation of [The Fold](/posts/theories/fold).

## ECHO
---
*I found some peace today, I grit my teeth*

*And swallow all of my pain*

*And selfish pride, I used to hide behind*

*Let me out let me out I'm singing*

*Let me out let me out I'm singing*

*I'm a liar and cheat in prison*

*Accused of telling the truth*

--- from [Future Leaders of the World - "Let Me Out"](https://www.youtube.com/watch?v=TZeWTI8jW6k)