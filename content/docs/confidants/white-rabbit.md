# The White Rabbit
## RECORD
---
```
Name: $REDACTED
Alias: ['The White Rabbit', and 6 unknown...]
Classification: Artificial Organic Computer
Race: Human
Gender: Female
Biological Age: 28 Earth Years
Chronological Age: N/A
SCAN Rank: | B B
           | C D
TIIN Rank: | B B
           | A D
Reviewer Rank: 3 stars
Organizations: 
  - The Corporation
Occupations:
  - Actress
  - Entrepreneur
  - Real Estate
Variables:
  $CAPABILITY: +0.95 | # Extremely talented.
  $TRUST:      +0.80 | # She is connected with many other confidants.
  $WOKE:       +0.80 | # Almost certainly.
```

## ECO
---
The White Rabbit has been buying-up real estate in [Brain](/posts/journal/2019.11.19.1/), in preparation for its total conversion into a Lonely Town.

She is responsible for the training and growth of many others like her.

## ECHO
---
*Dance, dance*

*We're falling apart to halftime*

*Dance, dance*

*And these are the lives you love to lead*

*Dance, this is the way they'd love*

*If they knew how misery loved me*

--- from [Fall Out Boy - "Dance, Dance"](https://www.youtube.com/watch?v=C6MOKXm8x50)

## PREDICTION
---
```
The Lion will contact her to kick-off the process on Ink's behalf.
```