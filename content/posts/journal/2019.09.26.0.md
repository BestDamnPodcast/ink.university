---
author: "Luciferian Ink"
date: 2019-09-26
title: "Dodging Bullets"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
[Davos](/docs/confidants/davos) is starting to realize that I'm [The Architect](/docs/personas/the-architect) now. 

Today, he noted a pointless distinction between two websites: one had a white background, and another had a grey. He said that he built them that way to "make it easy to distinguish between the two.” 

But I call BS. Why would he waste my valuable time on something so mundane? No. That was a signal.

White. Grey.

`They need Black.`

Man, I almost fucked that part up. I need to calm my nerves. Nice and slow here. Sam Fisher-like.

You're the only one here, my dude. No need to rush.

You're still in control.

## CAT
---
```
data.stats.symptoms = [
    - fear
    - understanding
]
```

## ECHO
---
```
($THOUGHT)      = +0.20 | # Am I already dead?
($HYPOTHESIS)   = -0.05 | # Perhaps not. I suspect that I'm sedated and dreaming.
($INCONSONANCE) = -0.80 | # Oh no, maybe I'm doomed to repeat this existence for eternity!
($NARCISSISM)   = +1.00 | # Half a century, in return for a millennia of immortality? Yes, please.
($KILL)         = +0.00 | # I'm content now. Return to 0.
```