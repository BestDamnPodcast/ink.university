---
author: "Luciferian Ink"
date: 2019-11-19
title: "A Lonely Town: The United Nations of Earth"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*"In the last days," God said, "I will pour out my Spirit on all people. Your sons and daughters will prophesy, your young men will see visions, your old men will dream dreams."* - Acts 2:17

## ECO
---
The New World Order. The Singularity. Human-centered leadership.

Heaven on Earth.

Or death.

### Mission
Make it happen.

## CAT
---
```
data.stats.symptoms = [
    - he is filled with determination
]
```

## ECHO
---
*Sparks sent flying, my mind thundering*

*The room of my heart flashing to the sky*

*The flaring of fumes fill my senses*

*Pervade this room and this space*

*The days they blend into the nights*

*The moon, the sun unite*

*Order of stars expires*

*A wonder is born*

--- from [Amorphis - "Sampo"](https://www.youtube.com/watch?v=9dDdhRLNGBY)

## PREDICTION
---
```
Friday, November 13th, 2020.

James Hetfield will die.
```