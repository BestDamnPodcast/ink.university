---
author: "Luciferian Ink"
date: 2019-11-20
title: "The Last Line"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Butternut Squash.

## ECO
---
YouTube had been recommending one of [Raven's](/docs/confidants/her) videos over and over for close to a week. [Fodder](/docs/personas/fodder) had already watched it multiple times (and he knew the algorithm's methods well), but he could not figure out why this video just kept coming back, day after day. This isn't how YouTube normally works.

Finally, he relented, and watched it once more. He discovered something that only context could highlight - this was verification! They predicted that there was still information to learn. And they were right!

Unfortunately, what YouTube didn't realize is that Raven had already performed verification in a half-dozen other ways. They were too late. And now, they had overplayed their hand.

Still, a verification is verification. Fodder wasn't going to complain. This certainly was the most poetic one of the bunch.

He opened the final page of his journal, and filled the last 3 lines with Raven's recipe:

*Butternut Squash*

*1 spicy item, 1 acidic, 1 sweet*

*=Ink*

And he closed this chapter of his life forever.

## CAT
---
```
data.stats.symptoms = [
    - devotion
]
```

## ECHO
---
The Ice Cream Truck music.

## PREDICTION
---
```
Beam me up, Scotty.
```