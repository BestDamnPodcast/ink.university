---
author: "Luciferian Ink"
date: 2018-09-23
title: "A Baseless Accusation"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## ECO
---
[Fodder](/docs/personas/fodder) was visiting his family one Sunday, as he often did. It was a full house; all of his siblings were there, and overall, it had been a fairly uneventful day. 

Nearing the end of the night, while Fodder and his siblings were preparing to leave, the Queen would make an unexpected allegation:

"Do you remember the time you hit me?"

"What are you talking about?" Fodder responded, "I never hit you!"

"Yes you did!" she persisted, "I remember it happening!"

"I would never attack you," Fodder continued. "I remember the time I hit [The Orchid](/docs/confidants/orchid), and I'll own that mistake. I was rightfully punished for it, too. But I never hit you!"

Fodder reflected upon the attack against his brother. It had come just days after he had been attacked by a fellow member of the basketball team at school, in eighth grade. In that instance, Fodder had been humiliated, because he hadn't fought back. He had allowed himself to be bullied, and he didn't retaliate at all. He was weak. He didn't stand up for himself.

His pent-up shame and anger would cause him to attack his brother just a few days later. This was the first and last time that Fodder ever attacked somebody. To his knowledge, anyway. People can suppress memories. People can hold false memories. But he really suspected that Mother was the one mistaken here:

"Mom, I really don't think this happened. What was the context?"

"I don't remember."

"Where were we?"

"I don't remember."

"What were we fighting about?"

"I don't remember."

"So, you don't remember a single detail, but you're choosing to believe that I attacked you? Even when I'm telling you that I don't remember it, and when I'm willing to admit that I HAVE attacked Orchid in the past?" he continued.

"I guess so," she replied, "I just know that it happened."

"I think I remember the instance you're talking about. You were mistaken then, as you are now."

Fodder would go on to describe an event from his youth. The Queen was yelling at him, as she often did, while [The Tradesman](/docs/confidants/father) sat idly by, saying nothing - as he often did. The specifics of the fight don't matter. What matters is that, at some point in the middle, the Queen flinched, halting her words completely.

"What?" she would say, shocked, "Were you going to hit me?"

"Huh?" Fodder replied, "Of course not!"

"Go on. Hit me! I know you wanted to do it! I saw you curl your fist."

"I didn't..." Fodder began, though it did not matter. He was quickly suppressed. The Queen had her beliefs, and she changed them for nobody. 

As she was doing today, while accusing Fodder of attacking her. She would continue to press the matter, until he finally exploded:

"I didn't fucking hit you!"

Shit. Now, he looked guilty. The whole family heard the accusations, and the whole family watched his response. Now, it didn't matter what he said; they would always remember him as the son who attacked his mother.

Over the coming months, Fodder would turn inward. Had he truly attacked his mother, and forgotten the event? Was she mistaken? He could not be sure about anything.

He would come to choose that they had each lived a separate timeline, and [that they were both right](/posts/theories/integration).

Mom would never change, though. She was immutable in her convictions.

Just like [God](/docs/confidants/dave) had designed her to be.

## CAT
---
```
data.stats.symptoms = [
    - anger
    - regret
    - humiliation
]
```

## ECHO
---
*Cause it's a bittersweet symphony this life*

*Trying to make ends meet, you're a slave to the money then you die.*

*I'll take you down the only road I've ever been down*

*You know the one that takes you to the places where all the veins meet, yeah.*

*No change, I can't change, I can't change, I can't change,*

*but I'm here in my mold, I am here in my mold.*

*But I'm a million different people from one day to the next*

*I can't change my mold, no, no, no, no, no, no, no*

--- from [The Verve - "Bittersweet Symphony"](https://www.youtube.com/watch?v=1lyu1KKwC74)