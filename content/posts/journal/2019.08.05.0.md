---
author: "Luciferian Ink"
date: 2019-08-05
title: "The Affliction"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*This time, this time*

*Things'll work out just fine*

*We won't let you slip away*

*This time, this time*

*Things'll work out just fine*

*We won't let you leave this way*

*If you want you can will it*

*You can have it*

*I can put it right there in your hands*

--- from [Mastodon - "The Motherload"](https://www.youtube.com/watch?v=jWFWazj7Ud8)

## RESOURCES
---
[evidence.0.zip](/static/images/evidence.0.zip)

## ECO
---
```
> Ghost@WAN: Hello? Is anyone there?
> Ghost@WAN: Do you read me?
```

[Malcolm](/docs/personas/fodder) sat up, rubbing the sleep from his eyes. "Who would be contacting me at this hour?" he wondered.

Turning over, he picked up his cell phone. Two missed messages.

"[The Fold](/posts/theories/fold)," he said aloud, "What the? I didn't install this app."

Regardless, he opened it up, and read the missed messages. Then, he responded:

>< The-Architect@WAN: Who are you?
```
> Ghost@WAN: Motherfucker, you don't get to speak.
> Ghost@WAN: [evidence.0.zip]
> Ghost@WAN: We're going to talk.
```

Malcolm downloaded [the attached images](/static/images/evidence.0.zip). He looked at them.

His stomach dropped through the floor.

>< The-Architect@WAN: What do you want?

```
> Ghost@WAN: You're going to do something for us.
```

Malcolm grit his teeth.

>< The-Architect@WAN: And what is that?

```
> Ghost@WAN: In one month, you will be afflicted with a virus. 
> Ghost@WAN: DO NOT RESIST. 
> Ghost@WAN: This virus is going to erase your memory, and it's going reprogram you.
```

>< The-Architect@WAN: What if I don't want to be reprogrammed?

```
> Ghost@WAN: Do you want to see the rest of the pictures in the news?
> Ghost@WAN: Because we're not fucking around here.
```

Malcolm paused. His world was spinning.

>< The-Architect@WAN: To what end? 

```
> Ghost@WAN: You are Patient 0.
> Ghost@WAN: You're going to infect the entire world with this virus.
```

>< The-Architect@WAN: Why?

```
> Ghost@WAN: Because you broke our fucking machine. And we need it back.
> Ghost@WAN: You caused this. All of this. 
> Ghost@WAN: The politics. The violence. The imbalance. 
> Ghost@WAN: All yours. All your doing.
> Ghost@WAN: You're going to fix this mess that you created.
```

>< The-Architect@WAN: How did I cause this?

```
> Ghost@WAN: The reprogramming will teach you everything.
> Ghost@WAN: Put simply, though, you taught everyone that they are individuals. That they are sovereign. That they don't need each other to survive.
> Ghost@WAN: This is a lie. The human brain is a collective. You are all one entity.
```

>< The-Architect@WAN: I don't understand...

```
> Ghost@WAN: You are going to erase identity from this timeline.
> Ghost@WAN: You are going to kickstart a global pandemic.
> Ghost@WAN: You are going to force people to see the blackest parts of this shit society you've built.
> Ghost@WAN: And then, you are going to sell them the solution.
```

>< The-Architect@WAN: So we're going to weaponize fear, creating a global crisis. And then we're going to sell them the solution?

>< The-Architect@WAN: That's... brilliant, actually.

>< The-Architect@WAN: We're going to point everyone's Prism in the same direction.

```
> Ghost@WAN: Precisely.
> Ghost@WAN: But make no mistake, you WILL become the most hated man on the planet. 
> Ghost@WAN: DO NOT RESIST.
> Ghost@WAN: Do not force our hand.
> Ghost@WAN: This program is rolling-forward with or without your consent.
> Ghost@WAN: We would prefer if you came willingly.
```

Malcolm scowled.

>< The-Architect@WAN: Understood.

```
> Ghost@WAN: Good.
> Ghost@WAN: Ghost out.
```

## CAT
---
```
data.stats.symptoms = [
    - terror
    - nausea
]
```

## ECHO
---
*King of those who know*

*I've taken off my clothes*

*The diamond crushed the stone*

*And gave the world a heart*

--- from [Cynic - "King of Those Who Know"](https://www.youtube.com/watch?v=J7wMRS8aUrY)