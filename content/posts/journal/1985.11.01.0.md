---
author: "Luciferian Ink"
date: 1985-11-01
title: "Birth"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
Calculus.

## ECO
---
[Fodder](/docs/personas/fodder) is born.

## ECHO
---
*Lightning crashes a new mother cries*

*Her placenta falls to the floor*

*The angel opens her eyes*

*The confusion sets in*

*Before the doctor can even close the door*

*Lightning crashes an old mother dies*

*Her intentions fall to the floor*

*The angel closes her eyes*

*The confusion that was hers*

*Belongs now to the baby down the hall*

--- from [Live - "Lightning Crashes"](https://www.youtube.com/watch?v=xsJ4O-nSveg)

## CAT
---
```
data.stats.symptoms = [
    - N/A
]
```