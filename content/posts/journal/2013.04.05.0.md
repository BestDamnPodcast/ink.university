---
author: "Luciferian Ink"
date: 2013-04-05
title: "The Abuser"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
A memory.

## ECO
---
In his later years with the healthcare corporation - and into his years with the cyber security company - [Fodder](/docs/personas/fodder) would come into his true form: the monster.

He would never escape this fate.

### The Anime Kid
In the last year of Fodder's term with the healthcare corporation, he would hire [a new kid](/docs/personas/fodder). The guy was bright - he had just graduated college - and he was optimistic. He watched a lot of anime - which Fodder had no interest in. He was gay - not that Fodder minded - but he was a little more sensitive than the other guys Fodder was used to interacting with.

And he didn't like Fodder's incessant taunting. 

At one point - perhaps a year after hiring - he came into Fodder's office, in tears, saying, "Why do you always make fun of me? What did I ever do to you? You make me feel stupid."

Fodder would flounder. He was incredibly confused - because narcissists do not understand their own actions. He would go on to formulate an apology - but it was wholly inadequate. And it didn't really fix the behavior.

Perhaps this this is a better apology:

```
The Ancient Magus Bride is a really cool anime. It actually reminds me A TON of the story I'm writing right now. Check it out.
P.S. - if you have an English dub, hook me up.
```

### The Strongman
[This man](/docs/confidants/strongman) was Fodder's best friend for many years. The two of them would attend concerts together, they would work at the same company (though he was subordinate to Fodder), and Fodder had even met his family a couple of times. Fodder really valued their friendship.

But Fodder was not a good boss. He put The Strongman into situations that never should have happened. He worked the Strongman to the bone - yet was never satisfied with the quality of his work. Nobody could ever live up to Fodder. Nobody could replace him. Nobody would ever replace him, as he ascended to his throne in Executive Management.

But Fodder would grow bored and frustrated, eventually leaving the company for a cyber security firm. The culture shock was so great that Fodder would almost immediately come to the realization that he had narcissistic personality disorder. He would seek therapy.

And he would call his best friend to discuss. To apologize. To rationalize.

That was the last time they ever spoke.

```
Thank you. For everything. I am sorry - and I am healed. I lose sleep over how I've treated you.
I hope you realize that you're the reason the new Coheed and Cambria album exists.
```

### The Wheels
[The Wheels](/docs/confidants/wheels) was a man that Fodder met only once, at the cyber security company. Inappropriately, another coworker had given him this nickname, on account of the fact that he was in a wheelchair. Fodder never called him this, though. Fodder really didn't treat him like a human at all; Fodder only cared about the "top secret project" that the two had been assigned to.

They would quickly clash. Fodder wanted to adopt every bleeding-edge piece of technology applicable to the need, while the Wheels wanted to play it safe, taking the problem one step at a time. He wanted to be steady, and sure - and he wanted to keep the scope of the project from growing out-of-hand.

In his frustration, Fodder would go on to gaslight this man to his superiors. In this way, he was able to get the Wheels removed from the project - leaving Fodder free to build it as he wishes.

Fodder would go on to over-complicate the project, miss the deadline, and ultimately, deliver nothing. The company would settle for a proprietary solution - which they would resell to customers - and they put the Wheels in charge of the initiative. This hurt Fodder's pride.

Fodder knows exactly how this man will remember him: he was the one who killed this really cool project, that he was very excited to take-on. Almost immediately after their first meeting, Fodder would begin to abuse him.

```
You were the first to make me reconsider my left-leaning political views. 
You might even call me, like, 40% Republican, now.
Also, 3 victims is a verification. So... congrats, I guess? # Verified.
```

## CAT
---
```
data.stats.symptoms = [
    - regret
]
```

## ECHO
---
*Somewhere outside that*

*Broken township*

*I'll find the guy that's left me in my grave*

*Ain't no sign, no direction*

*But the people know the way*

--- from [Country Lips - "Pretty Pictures"](https://www.youtube.com/watch?v=Fyyc8GhEF5s)