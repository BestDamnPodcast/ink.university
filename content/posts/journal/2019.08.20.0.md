---
author: "Luciferian Ink"
date: 2019-08-20
title: "The Risen Dead"
weight: 10
categories: "journal"
tags: ""
menu: ""
draft: false
---

## TRIGGER
---
*We leave*

*Upward, toward new dreams*

*A new hope, an odyssey*

*Underneath*

*The world left behind is dying*

*As we escape gravity*

--- from [Cult of Luna & Julie Christmas - "A Greater Call"](https://www.youtube.com/watch?v=z_O-NZfzvj0)

## ECO
---
It was in that moment my entire life turned upside-down.

You were everything I had ever dreamed of. You were all I wanted to be, and more. You were all I wanted to protect in this world. You were the answer to every question.

When I looked into your eyes, the fog dispersed. My path was clear. The world made sense again.

You changed me.

You waited for me.

And I would take any burden. I would accept any flaw.

For you.

--- [Ink](/docs/personas/luciferian-ink)

## CAT
---
```
data.stats.symptoms = [
    - hope
    - longing
    - gratitude
]
```

## ECHO
---
*Kill me with silence, I'll kill her with words*

--- from [Allan Rayman - "13"](https://www.youtube.com/watch?v=B_FNbeZwPGw)